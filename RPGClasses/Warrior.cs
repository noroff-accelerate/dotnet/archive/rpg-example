﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class Warrior : ICharacter
    {
        public string Name { get; set; }
        public int Mana { get; set; }
        public int Health { get; set; }
        public int ArmorRating { get; set; }

        public int X { get; private set; }

        public int Y { get; private set; }

        public void Attack(ICharacter character2)
        {
            throw new NotImplementedException();
        }

        public void Move(int x, int y)
        {
            throw new NotImplementedException();
        }
    }
}
