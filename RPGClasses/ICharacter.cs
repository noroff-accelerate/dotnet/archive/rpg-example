﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public interface ICharacter
    {
        string Name { get; set; }
        int Mana { get; set; }
        int Health { get; set; }
        int ArmorRating { get; set; }
        /// <summary>
        /// Position in the X axis
        /// </summary>
        int X { get; }
        /// <summary>
        /// Position in the Y axis
        /// </summary>
        int Y { get; }

        void Attack(ICharacter character2);

        void Move(int x, int y);

    }
}
