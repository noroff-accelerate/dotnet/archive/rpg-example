﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class LizardWizard : Wizard
    {
        public LizardWizard()
        {
        }
        public LizardWizard(string name, int mana, int health, int armorRating) : base(name, mana, health, armorRating)
        {
        }

        public override void Attack(ICharacter character2)
        {
            base.Attack(character2);
        }
    }
}
